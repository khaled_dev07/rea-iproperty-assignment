package com.rea.ipropertyassignment.domain.model

import com.google.gson.annotations.SerializedName

data class PropertyDetails(
    @SerializedName("channels") val channels: List<String>,
    @SerializedName("kind") val kind: String,
    @SerializedName("id") val id: String,
    @SerializedName("shareLink") val shareLink: String,
    @SerializedName("title") val title: String,
    @SerializedName("tier") val tier: Int,
    @SerializedName("propertyType") val propertyType: String,
    @SerializedName("propertyCatType") val propertyCatType: String,
    @SerializedName("active") val active: Boolean,
    @SerializedName("prices") val prices: List<Prices>,
    @SerializedName("medias") val medias: List<Cover>,
    @SerializedName("cover") val cover: Cover?,
    @SerializedName("description") val description: String,
    @SerializedName("updatedAt") val updatedAt: String,
    @SerializedName("address") val address: Address,
    @SerializedName("multilanguagePlace") val multiLanguagePlace: MultiLanguagePlace,
    @SerializedName("featureDescription") val featureDescription: String,
    @SerializedName("referenceCode") val referenceCode: String,
    @SerializedName("attributes") val attributes: Attributes,
    @SerializedName("listers") val listers: List<Listers>,
    @SerializedName("organisations") val organisations: List<Organisations>
) {
    val coverThumbnailUrl: String?
        get() = cover?.thumbnailUrl

    val mediaSize: String
        get() = String.format("%d", medias.size)

    val price: String
        get() = String.format("%s %d", prices[0].currency, prices[0].min)

    val address2: String
        get() = String.format(
            "%s %s", multiLanguagePlace.enGB?.levelTwo, multiLanguagePlace.enGB?.levelOne,
        )

    val space: String
        get() = String.format("built-up Size: %s", attributes.builtUp)


    val landTitle: String
        get() = attributes.landTitleType ?: ""

    val unitType: String
        get() = attributes.unitType?.let { return it } ?: ""

    val tenure: String
        get() = attributes.tenure ?: ""

    val furnishing: String
        get() = attributes.furnishing ?: ""


    val agentName: String
        get() = listers[0].name ?: ""

    val agentProfileImageUrl: String
        get() = listers[0].image?.thumbnailUrl ?: ""

    val agentTitle: String
        get() = organisations[0].name
}
