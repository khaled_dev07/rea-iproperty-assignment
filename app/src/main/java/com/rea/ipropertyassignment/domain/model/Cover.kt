package com.rea.ipropertyassignment.domain.model

import com.google.gson.annotations.SerializedName

data class Cover(
	@SerializedName("type") val type: String,
	@SerializedName("url") val url: String?,
	@SerializedName("thumbnailUrl") val thumbnailUrl: String,
	@SerializedName("urlTemplate") val urlTemplate: String
)