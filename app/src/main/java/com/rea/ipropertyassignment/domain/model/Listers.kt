package com.rea.ipropertyassignment.domain.model

import com.google.gson.annotations.SerializedName

data class Listers(
	@SerializedName("id") val id: Int?,
	@SerializedName("type") val type: String?,
	@SerializedName("name") val name: String?,
	@SerializedName("contact") val contact: Contact?,
	@SerializedName("image") val image: Cover?
)