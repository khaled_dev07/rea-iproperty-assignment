package com.rea.ipropertyassignment.domain.model

import com.google.gson.annotations.SerializedName

data class Properties(
    @SerializedName("totalCount") val totalCount: Int,
    @SerializedName("nextPageToken") val nextPageToken: Int,
    @SerializedName("items") val items: List<Item>
)