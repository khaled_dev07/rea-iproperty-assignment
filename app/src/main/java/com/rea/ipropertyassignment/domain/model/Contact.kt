package com.rea.ipropertyassignment.domain.model

import com.google.gson.annotations.SerializedName

data class Contact(
	@SerializedName("phones") val phones: List<Phones>
)