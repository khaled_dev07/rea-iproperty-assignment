package com.rea.ipropertyassignment.domain.model

import com.google.gson.annotations.SerializedName

data class MultiLanguagePlace(
    @SerializedName("en-GB") val enGB: EnGB?
)