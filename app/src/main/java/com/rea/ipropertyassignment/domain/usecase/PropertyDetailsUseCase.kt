package com.rea.ipropertyassignment.domain.usecase

import android.util.Log
import com.rea.ipropertyassignment.data.repository.Repository
import com.rea.ipropertyassignment.domain.model.PropertyDetails
import com.rea.ipropertyassignment.domain.model.usecase.PropertyDetailsRequestParams
import com.rea.ipropertyassignment.domain.usecase.base.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class PropertyDetailsUseCase @Inject constructor(
    private val repository: Repository
) : SingleUseCase<PropertyDetails, PropertyDetailsRequestParams>() {

    override fun executeUseCase(params: PropertyDetailsRequestParams): Single<PropertyDetails> {
        Log.d(TAG, "PropertyDetailsUseCase() called  with: params = [$params]")
        return repository.getPropertyDetails("1")
    }

    companion object {
        private val TAG = PropertyDetailsUseCase::class.java.simpleName
    }
}