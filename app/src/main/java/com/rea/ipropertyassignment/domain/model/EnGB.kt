package com.rea.ipropertyassignment.domain.model

import com.google.gson.annotations.SerializedName

data class EnGB(
    @SerializedName("level1")
    val levelOne: String,
    @SerializedName("level2")
    val levelTwo: String,
    @SerializedName("level3")
    val levelThree: String?
)