package com.rea.ipropertyassignment.domain.model

import com.google.gson.annotations.SerializedName

data class Phones(
	@SerializedName("label") val label: String,
	@SerializedName("number") val number: String
)