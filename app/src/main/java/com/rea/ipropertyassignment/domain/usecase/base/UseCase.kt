package com.rea.ipropertyassignment.domain.usecase.base

abstract class UseCase<T, in P> {

    abstract fun executeUseCase(params: P): T
}