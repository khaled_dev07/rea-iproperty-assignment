package com.rea.ipropertyassignment.domain.network

import com.rea.ipropertyassignment.domain.model.Properties
import com.rea.ipropertyassignment.domain.model.PropertyDetails
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiInterface {

//    @Headers("mock:true")
    @GET("search")
    fun searchProperty(@Query("page") page: Int, @Query("pageSize") pageSize: Int): Single<Properties>

//    @Headers("mock:true")
    @GET("details")
    fun getPropertyDetails(@Query("id") id: String): Single<PropertyDetails>
}