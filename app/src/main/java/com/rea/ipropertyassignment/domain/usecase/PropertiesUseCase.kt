package com.rea.ipropertyassignment.domain.usecase

import android.util.Log
import com.rea.ipropertyassignment.data.repository.Repository
import com.rea.ipropertyassignment.domain.model.Properties
import com.rea.ipropertyassignment.domain.model.usecase.NetworkRequestParams
import com.rea.ipropertyassignment.domain.usecase.base.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class PropertiesUseCase @Inject constructor(
    private val repository: Repository
) : SingleUseCase<Properties, NetworkRequestParams>() {

    override fun executeUseCase(params: NetworkRequestParams): Single<Properties> {
        Log.d(TAG, "PropertiesUseCase() called  with: params = [$params]")
        return repository.searchProperty(params.page, params.pageSize)
    }

    companion object {
        private val TAG = PropertiesUseCase::class.java.simpleName
    }
}