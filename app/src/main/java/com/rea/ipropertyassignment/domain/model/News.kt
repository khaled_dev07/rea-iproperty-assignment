package com.rea.ipropertyassignment.domain.model

data class News(
	val id: Int = 0,
	val title: String = "",
	val imageUrl: String = ""
)