package com.rea.ipropertyassignment.domain.usecase.base

import com.rea.ipropertyassignment.domain.mapper.ErrorMapper
import com.rea.ipropertyassignment.domain.model.usecase.ErrorModel
import com.rea.ipropertyassignment.domain.model.usecase.UseCaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

abstract class SingleUseCase<T, in P> : UseCase<Single<T>, P>() {

    private val errorMapper = ErrorMapper()

    fun execute(
        params: P,
        compositeDisposable: CompositeDisposable,
        onResponse: (UseCaseResponse<T>) -> Unit
    ): Disposable {
        return executeUseCase(params)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    onResponse(UseCaseResponse.SuccessResponse(result))
                },
                { error ->
                    val errorModel: ErrorModel = errorMapper.mapToDomainErrorException(error)
                    onResponse(UseCaseResponse.ErrorResponse(errorModel))
                }
            )
            .also { compositeDisposable.add(it) }
    }
}