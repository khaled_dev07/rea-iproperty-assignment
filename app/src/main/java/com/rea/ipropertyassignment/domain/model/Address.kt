package com.rea.ipropertyassignment.domain.model

import com.google.gson.annotations.SerializedName

data class Address(
	@SerializedName("formattedAddress") val formattedAddress: String,
	@SerializedName("lat") val lat: Double,
	@SerializedName("lng") val lng: Double
)