package com.rea.ipropertyassignment.domain.model

import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("channels") val channels: List<String>,
    @SerializedName("kind") val kind: String,
    @SerializedName("id") val id: String,
    @SerializedName("shareLink") val shareLink: String,
    @SerializedName("title") val title: String,
    @SerializedName("active") val active: Boolean,
    @SerializedName("tier") val tier: Int,
    @SerializedName("propertyType") val propertyType: String,
    @SerializedName("prices") val prices: List<Prices>,
    @SerializedName("cover") val cover: Cover?,
    @SerializedName("medias") val medias: List<Cover>,
    @SerializedName("updatedAt") val updatedAt: String,
    @SerializedName("publishedAt") val publishedAt: String,
    @SerializedName("address") val address: Address?,
    @SerializedName("multilanguagePlace") val multiLanguagePlace: MultiLanguagePlace,
    @SerializedName("referenceCode") val referenceCode: String,
    @SerializedName("attributes") val attributes: Attributes,
    @SerializedName("listers") val listers: List<Listers>?,
    @SerializedName("organisations") val organisations: List<Organisations>?
)