package com.rea.ipropertyassignment.domain.model.usecase

import com.rea.ipropertyassignment.utils.Constants.PAGE_SIZE

data class NetworkRequestParams(val page: Int = 1, val pageSize: Int = PAGE_SIZE)