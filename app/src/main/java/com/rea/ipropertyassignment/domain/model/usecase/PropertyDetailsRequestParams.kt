package com.rea.ipropertyassignment.domain.model.usecase

data class PropertyDetailsRequestParams(val id: String = "0")
