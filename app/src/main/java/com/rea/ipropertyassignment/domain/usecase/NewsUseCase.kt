package com.rea.ipropertyassignment.domain.usecase

import android.util.Log
import com.rea.ipropertyassignment.data.repository.Repository
import com.rea.ipropertyassignment.domain.model.News
import com.rea.ipropertyassignment.domain.model.usecase.NetworkRequestParams
import com.rea.ipropertyassignment.domain.usecase.base.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class NewsUseCase @Inject constructor(
    private val repository: Repository
) : SingleUseCase<List<News>, NetworkRequestParams>() {

    override fun executeUseCase(params: NetworkRequestParams): Single<List<News>> {
        Log.d(TAG, "NewsUseCase() called  with: params = [$params]")
        return repository.getAvailableNews()
    }

    companion object {
        private val TAG = NewsUseCase::class.java.simpleName
    }
}