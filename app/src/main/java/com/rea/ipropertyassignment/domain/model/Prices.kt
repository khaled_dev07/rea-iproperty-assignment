package com.rea.ipropertyassignment.domain.model

import com.google.gson.annotations.SerializedName

data class Prices(
	@SerializedName("type") val type: String,
	@SerializedName("currency") val currency: String,
	@SerializedName("max") val max: Int,
	@SerializedName("min") val min: Int
)