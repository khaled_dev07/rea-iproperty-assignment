package com.rea.ipropertyassignment.domain.model

import com.google.gson.annotations.SerializedName

data class Attributes(
	@SerializedName("landArea") val landArea: String?,
	@SerializedName("bathroom") val bathroom: String?,
	@SerializedName("bedroom") val bedroom: String?,
	@SerializedName("carPark") val carPark: String?,
	@SerializedName("builtUp") val builtUp: String?,
	@SerializedName("landTitleType") val landTitleType: String?,
	@SerializedName("furnishing") val furnishing: String?,
	@SerializedName("tenure") val tenure: String?,
	@SerializedName("unitType") val unitType: String?,
	@SerializedName("sizeUnit") val sizeUnit: String?,

	@SerializedName("downloadUrl") val downloadUrl: String?,
	@SerializedName("buildingId") val buildingId: Int?,
	@SerializedName("township") val township: String?
)