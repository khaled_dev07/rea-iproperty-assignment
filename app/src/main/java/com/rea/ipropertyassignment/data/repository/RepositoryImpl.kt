package com.rea.ipropertyassignment.data.repository

import com.rea.ipropertyassignment.data.provider.NewsProvider
import com.rea.ipropertyassignment.domain.model.News
import com.rea.ipropertyassignment.domain.model.Properties
import com.rea.ipropertyassignment.domain.model.PropertyDetails
import com.rea.ipropertyassignment.domain.network.ApiInterface
import io.reactivex.Single
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    private val apiInterface: ApiInterface,
) : Repository {

    override fun getAvailableNews(): Single<List<News>> {
        return Single.just(NewsProvider.getNewsList())
    }

    override fun searchProperty(page: Int, pageSize: Int): Single<Properties> {
        return apiInterface.searchProperty(page, pageSize)
    }

    override fun getPropertyDetails(id: String): Single<PropertyDetails> {
        return apiInterface.getPropertyDetails(id)
    }
}