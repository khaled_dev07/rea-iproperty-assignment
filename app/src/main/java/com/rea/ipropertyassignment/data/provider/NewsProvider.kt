package com.rea.ipropertyassignment.data.provider

import com.rea.ipropertyassignment.domain.model.News

object NewsProvider {

    fun getNewsList(): List<News> {
        var id = 1
        return listOf(
            News(
                id = id++,
                title = "Continuous Integration and Delivery pipeline mistakes",
                imageUrl = "https://cdn.rea-group.com/wp-content/uploads/2021/02/26172948/Blacktown-0055-768x433.jpg"
            ),
            News(
                id = id++,
                title = "CEO Owen Wilson in The Big Issue",
                imageUrl = "https://cdn.rea-group.com/wp-content/uploads/2021/02/11121927/Owen-and-Phil-sell-the-Big-Issue_2-1-169-768x432.jpg"
            ),
            News(
                id = id++,
                title = "Career Deep Dive: Data & Analytics",
                imageUrl = "https://cdn.rea-group.com/wp-content/uploads/2021/02/10120038/KateHart.jpeg"
            ),
            News(
                id = id++,
                title = "The way we work at REA: a hybrid approach",
                imageUrl = "https://cdn.rea-group.com/wp-content/uploads/2021/02/03103623/Hybrid-Working-1-1920x1080.png"
            ),
            News(
                id = id++,
                title = "Running virtual hack days",
                imageUrl = "https://cdn.rea-group.com/wp-content/uploads/2020/12/03121931/talk-data-to-me-vaporwave-1920x1080.png"
            ),
            News(
                id = id++,
                title = "Building DesignOps at REA Group",
                imageUrl = "https://cdn.rea-group.com/wp-content/uploads/2020/11/16152434/Chan-Armstrong-09-1-1-1920x1080.jpg"
            ),
            News(
                id = id++,
                title = "Career Deep Dive: Product",
                imageUrl = "https://cdn.rea-group.com/wp-content/uploads/2020/10/27185236/simon-hope-melbourne-4-1920x1080.jpg"
            ),
            News(
                id = id++,
                title = "Our Sales Leaders of Tomorrow",
                imageUrl = "https://cdn.rea-group.com/wp-content/uploads/2020/10/28135308/Sales-Leaders-Brady-Bunch-002.jpeg"
            ),
            News(
                id = id++,
                title = "Celebrating the success of our SEO team",
                imageUrl = "https://cdn.rea-group.com/wp-content/uploads/2020/10/27184337/SEO-team-1920x1080.png"
            ),
            News(
                id = id,
                title = "REA Group’s commitment to carbon neutrality",
                imageUrl = "https://cdn.rea-group.com/wp-content/uploads/2020/10/16082102/Sustainability_LinkedIn_5.png"
            ),
        )
    }
}