package com.rea.ipropertyassignment.data.repository

import com.rea.ipropertyassignment.domain.model.News
import com.rea.ipropertyassignment.domain.model.Properties
import com.rea.ipropertyassignment.domain.model.PropertyDetails
import io.reactivex.Single

interface Repository {

    fun getAvailableNews(): Single<List<News>>

    fun searchProperty(page: Int, pageSize: Int): Single<Properties>

    fun getPropertyDetails(id: String): Single<PropertyDetails>
}