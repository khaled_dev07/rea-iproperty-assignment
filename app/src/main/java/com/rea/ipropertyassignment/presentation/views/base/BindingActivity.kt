package com.rea.ipropertyassignment.presentation.views.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.lifecycle.LifecycleObserver
import androidx.viewbinding.ViewBinding
import dagger.android.support.DaggerAppCompatActivity

abstract class BindingActivity<ViewBindingType : ViewBinding> : DaggerAppCompatActivity(),
    LifecycleObserver {

    private var _binding: ViewBindingType? = null
    protected val binding
        get() = requireNotNull(_binding)


    abstract fun setupViewBinding(inflater: LayoutInflater): ViewBindingType

    abstract fun onActivityCreated(savedInstanceState: Bundle?)

    abstract fun initViewModel()

    abstract fun fetchData()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = setupViewBinding(layoutInflater)
        setContentView(requireNotNull(_binding).root)
        lifecycle.addObserver(this)

        onActivityCreated(savedInstanceState)
        initViewModel()

        fetchData()
    }

    override fun onDestroy() {
        _binding = null
        lifecycle.removeObserver(this)
        super.onDestroy()
    }
}