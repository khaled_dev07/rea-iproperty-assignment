package com.rea.ipropertyassignment.presentation.views.main.home

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.rea.ipropertyassignment.domain.model.News
import com.rea.ipropertyassignment.presentation.listeners.RecyclerViewItemClickListener

class NewsAdapter(
    private val itemClickListener: RecyclerViewItemClickListener<News>
) : ListAdapter<News, NewsViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder.create(parent)
    }

    override fun onBindViewHolder(viewHolder: NewsViewHolder, position: Int) {
        viewHolder.bind(getItem(position), itemClickListener)
    }


    companion object {
        val DiffCallback = object : DiffUtil.ItemCallback<News>() {
            override fun areItemsTheSame(
                oldItem: News,
                newItem: News
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: News,
                newItem: News
            ): Boolean {
                return oldItem.id == newItem.id
                        && oldItem.title == newItem.title
                        && oldItem.imageUrl == newItem.imageUrl
            }
        }
    }
}