package com.rea.ipropertyassignment.presentation.views.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleObserver
import androidx.viewbinding.ViewBinding
import dagger.android.support.DaggerFragment

abstract class BindingFragment<ViewBindingType : ViewBinding> : DaggerFragment(),
    LifecycleObserver {

    private var _binding: ViewBindingType? = null
    protected val binding
        get() = requireNotNull(_binding)


    abstract fun setupViewBinding(
        inflater: LayoutInflater, container: ViewGroup?,
    ): ViewBindingType

    abstract fun onFragmentCreated(savedInstanceState: Bundle?)

    abstract fun initViewModel()

    abstract fun fetchData()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = setupViewBinding(inflater, container)
        return requireNotNull(_binding).root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewLifecycleOwner.lifecycle.addObserver(this)

        onFragmentCreated(savedInstanceState)
        initViewModel()

        fetchData()
    }

    override fun onDestroyView() {
        _binding = null
        viewLifecycleOwner.lifecycle.removeObserver(this)
        super.onDestroyView()
    }
}