package com.rea.ipropertyassignment.presentation.views.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rea.ipropertyassignment.R
import com.rea.ipropertyassignment.databinding.PropertiesListItemBinding
import com.rea.ipropertyassignment.presentation.listeners.RecyclerViewItemClickListener
import com.rea.ipropertyassignment.presentation.model.UiItem


class PropertiesViewHolder(private val binding: PropertiesListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: UiItem?, itemClickListener: RecyclerViewItemClickListener<UiItem>) {
        if (item != null) {
            if (!item.listers.isNullOrEmpty()) {
                binding.txtName.text = item.listers[0].name
                item.listers[0].image?.let {
                    Glide.with(binding.imgProfile.context)
                        .load(it.thumbnailUrl)
                        .centerCrop()
                        .placeholder(R.drawable.ic_profile_inactive)
                        .error(R.drawable.ic_profile_inactive)
                        .into(binding.imgProfile)
                }
            }

            item.cover?.let {
                Glide.with(binding.imgBackground.context)
                    .load(it.thumbnailUrl)
                    .centerCrop()
                    .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                    .error(android.R.drawable.stat_notify_error)
                    .into(binding.imgBackground)
            }

            binding.txtImagesCount.text = item.medias.size.toString()

            binding.txtPrice.text = binding.root.context.getString(
                R.string.price,
                item.prices[0].currency,
                item.prices[0].min
            )

            binding.txtAddress1.text = item.title
            binding.txtAddress2.text = binding.root.context.getString(
                R.string.full_address,
                item.multiLanguagePlace.enGB?.levelTwo,
                item.multiLanguagePlace.enGB?.levelOne,
            )

            binding.txtType.text = item.propertyType
            binding.txtSpace.text = binding.root.context.getString(
                R.string.built_up,
                item.attributes.builtUp
            )

            item.attributes.furnishing?.let {
                binding.txtFurnishing.text = binding.root.context.getString(
                    R.string.furnishing,
                    item.attributes.furnishing
                )
            }

            binding.txtBedrooms.text = item.attributes.bedroom
            binding.txtBathrooms.text = item.attributes.bathroom
            binding.txtCarparks.text = item.attributes.carPark

            itemView.setOnClickListener {
                itemClickListener.onItemClicked(item, adapterPosition)
            }
        }
    }

    companion object {
        fun create(parent: ViewGroup): PropertiesViewHolder {
            val binding = PropertiesListItemBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
            return PropertiesViewHolder(binding)
        }
    }

}