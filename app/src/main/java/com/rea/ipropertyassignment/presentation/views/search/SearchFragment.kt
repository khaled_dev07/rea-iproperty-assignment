package com.rea.ipropertyassignment.presentation.views.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rea.ipropertyassignment.R
import com.rea.ipropertyassignment.databinding.FragmentSearchBinding
import com.rea.ipropertyassignment.presentation.listeners.RecyclerViewItemClickListener
import com.rea.ipropertyassignment.presentation.model.ApiState
import com.rea.ipropertyassignment.presentation.model.UiItem
import com.rea.ipropertyassignment.presentation.model.UiProperties
import com.rea.ipropertyassignment.presentation.views.base.BindingFragment
import com.rea.ipropertyassignment.presentation.views.details.DetailsFragment
import com.rea.ipropertyassignment.utils.EndlessRecyclerViewScrollListener
import com.rea.ipropertyassignment.utils.Navigator
import com.rea.ipropertyassignment.utils.extensions.*
import javax.inject.Inject

class SearchFragment : BindingFragment<FragmentSearchBinding>(),
    RecyclerViewItemClickListener<UiItem> {

    companion object {
        private const val PROPERTY_NAME = "property_name"

        fun newInstance(propertyName: String): SearchFragment {
            val fragment = SearchFragment()
            val bundle = Bundle()
            bundle.putString(PROPERTY_NAME, propertyName)
            fragment.arguments = bundle
            return fragment
        }
    }

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    private val viewModel: SearchViewModel by lazy { injectViewModel(factory) }

    private lateinit var adapter: PropertiesAdapter


    override fun setupViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentSearchBinding {
        return FragmentSearchBinding.inflate(inflater, container, false)
    }

    override fun onFragmentCreated(savedInstanceState: Bundle?) {
        arguments?.let {
            val name = it.getString(PROPERTY_NAME, "")
            viewModel.setSearchedProperty(name)
        }

        binding.fragmentSearchHeader.imgBack.setOnClickListener {
            Navigator.with(activity).popFragment()
        }

        setupPropertiesRecyclerView()
    }

    private fun setupPropertiesRecyclerView() {
        adapter = PropertiesAdapter(arrayListOf(), this)

        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.fragmentSearchContent.rvProperties.setupLayout(layoutManager, false)
        binding.fragmentSearchContent.rvProperties.adapter = adapter

        binding.fragmentSearchContent.rvProperties.addOnScrollListener(object :
            EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                viewModel.getProperties(page)
            }
        })
    }

    override fun initViewModel() {
        with(viewModel) {
            searchedPropertyName.observe(
                this@SearchFragment,
                { name -> updatePropertyName(name) }
            )
            propertiesList.observe(
                this@SearchFragment,
                { state -> handleResponse(state) }
            )
        }
    }

    override fun fetchData() {
        viewModel.getProperties()
    }

    private fun updatePropertyName(name: String?, totalCount: Int = 0) {
        name?.let {
            binding.fragmentSearchHeader.txtSearch.text = name
            binding.txtPropertiesCount.text = getString(
                R.string.properties_count,
                totalCount,
                name
            )
        }
    }

    private fun handleResponse(state: ApiState<UiProperties>) {
        when (state) {
            is ApiState.Loading -> onLoading()
            is ApiState.Error -> onErrorResponse(state.message)
            is ApiState.Success -> onSuccessResponse(state.data)
        }
    }

    private fun onLoading() {
        showLoading()
    }

    private fun onErrorResponse(message: String) {
        hideLoading()
        hidePropertiesList()

        activity?.toast(message = message)
    }

    private fun onSuccessResponse(data: UiProperties) {
        hideLoading()

        updatePropertyName(viewModel.searchedPropertyName.value, data.totalCount)
        showPropertiesList()

        adapter.addItems(data.items)
    }


    private fun showLoading() {
        binding.fragmentSearchContent.rvProperties.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        binding.fragmentSearchContent.rvProperties.visibility = View.GONE
    }

    private fun showPropertiesList() {
        binding.txtPropertiesCount.visibility = View.VISIBLE
        binding.fragmentSearchContent.rvProperties.visibility = View.VISIBLE
    }

    private fun hidePropertiesList() {
        binding.txtPropertiesCount.visibility = View.GONE
        binding.fragmentSearchContent.rvProperties.visibility = View.GONE
    }


    override fun onItemClicked(item: UiItem, position: Int) {
        Navigator.with(activity)
            .open(DetailsFragment.newInstance(item.id), true)
    }
}