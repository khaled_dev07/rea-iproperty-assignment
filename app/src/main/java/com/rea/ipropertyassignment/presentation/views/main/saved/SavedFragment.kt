package com.rea.ipropertyassignment.presentation.views.main.saved

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.rea.ipropertyassignment.databinding.FragmentSavedBinding
import com.rea.ipropertyassignment.presentation.views.base.BindingFragment
import com.rea.ipropertyassignment.presentation.views.main.home.HomeFragment

class SavedFragment : BindingFragment<FragmentSavedBinding>() {

    companion object {
        fun newInstance() = HomeFragment()
    }

    override fun setupViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentSavedBinding {
        return FragmentSavedBinding.inflate(inflater, container, false)
    }

    override fun onFragmentCreated(savedInstanceState: Bundle?) {
    }

    override fun initViewModel() {
    }

    override fun fetchData() {
    }

}