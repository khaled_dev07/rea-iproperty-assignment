package com.rea.ipropertyassignment.presentation.model

data class UiOrganisations(
	val id: Int,
	val type: String,
	val name: String
)