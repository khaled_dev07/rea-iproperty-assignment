package com.rea.ipropertyassignment.presentation.model

data class UiMultiLanguagePlace(
    val enGB: UiEnGB?
)