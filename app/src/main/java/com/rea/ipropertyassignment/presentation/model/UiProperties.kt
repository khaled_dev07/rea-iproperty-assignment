package com.rea.ipropertyassignment.presentation.model

data class UiProperties(
    val totalCount: Int,
    val nextPageToken: Int,
    val items: List<UiItem>
)