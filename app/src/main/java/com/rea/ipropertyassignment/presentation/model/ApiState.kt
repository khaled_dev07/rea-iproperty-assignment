package com.rea.ipropertyassignment.presentation.model

sealed class ApiState<out T> {
    object Loading : ApiState<Nothing>()
    data class Success<T>(val data: T) : ApiState<T>()
    data class Error(val message: String, val cause: Exception? = null) : ApiState<Nothing>()
}