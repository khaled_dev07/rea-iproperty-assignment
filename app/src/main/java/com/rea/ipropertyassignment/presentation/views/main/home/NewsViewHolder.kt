package com.rea.ipropertyassignment.presentation.views.main.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rea.ipropertyassignment.databinding.NewsListItemBinding
import com.rea.ipropertyassignment.domain.model.News
import com.rea.ipropertyassignment.presentation.listeners.RecyclerViewItemClickListener

class NewsViewHolder(private val binding: NewsListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: News?, itemClickListener: RecyclerViewItemClickListener<News>) {
        if (item != null) {
            binding.txtNews.text = item.title
            Glide.with(binding.imgNews.context)
                .load(item.imageUrl)
                .centerCrop()
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .error(android.R.drawable.stat_notify_error)
                .into(binding.imgNews)

            itemView.setOnClickListener {
                itemClickListener.onItemClicked(item, adapterPosition)
            }
        }
    }

    companion object {
        fun create(parent: ViewGroup): NewsViewHolder {
            val binding = NewsListItemBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
            return NewsViewHolder(binding)
        }
    }

}