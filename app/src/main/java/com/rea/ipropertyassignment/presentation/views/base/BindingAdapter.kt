package com.rea.ipropertyassignment.presentation.views.base

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.rea.ipropertyassignment.R

@BindingAdapter("imageUrl")
fun loadImage(imageView: ImageView, url: String?) {
    Glide.with(imageView.context)
        .load(url)
        .centerCrop()
        .placeholder(android.R.drawable.progress_indeterminate_horizontal)
        .error(R.drawable.ic_profile_inactive)
        .into(imageView)
}
