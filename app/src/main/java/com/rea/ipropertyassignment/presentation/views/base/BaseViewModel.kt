package com.rea.ipropertyassignment.presentation.views.base

import android.util.Log
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : ViewModel() {

    companion object {
        private val TAG = BaseViewModel::class.java.simpleName
    }

    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()

        compositeDisposable.dispose()
        compositeDisposable.clear()

        Log.d(TAG, "onCleared() disposes all the previously contained Disposables ")
    }
}