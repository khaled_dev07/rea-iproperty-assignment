package com.rea.ipropertyassignment.presentation.views.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.MapView
import com.rea.ipropertyassignment.R
import com.rea.ipropertyassignment.databinding.FragmentDetailsBinding
import com.rea.ipropertyassignment.presentation.model.ApiState
import com.rea.ipropertyassignment.presentation.model.UiPropertyDetails
import com.rea.ipropertyassignment.utils.Navigator
import com.rea.ipropertyassignment.utils.extensions.init
import com.rea.ipropertyassignment.utils.extensions.injectViewModel
import com.rea.ipropertyassignment.utils.extensions.showMarker
import com.rea.ipropertyassignment.utils.extensions.toast
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class DetailsFragment : DaggerFragment() {

    companion object {
        private const val PROPERTY_ID = "property_id"

        fun newInstance(propertyId: String): DetailsFragment {
            val fragment = DetailsFragment()
            val bundle = Bundle()
            bundle.putString(PROPERTY_ID, propertyId)
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var binding: FragmentDetailsBinding
    private lateinit var mapView: MapView

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    private val viewModel: DetailsViewModel by lazy { injectViewModel(factory) }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()
        initMap(savedInstanceState)
        initViewModel()

        arguments?.let {
            val id = it.getString(PROPERTY_ID, "0")
            viewModel.getPropertyDetails(id)
        }
    }

    private fun initUi() {
        binding.layoutDetailsHeader.imgBack.setOnClickListener {
            Navigator.with(activity).popFragment()
        }
    }

    private fun initMap(savedInstanceState: Bundle?) {
        activity?.let {
            mapView = binding.fragmentDetailsContent.mapView
            mapView.init(it.applicationContext, savedInstanceState)
        }
    }

    private fun initViewModel() {
        with(viewModel) {
            propertyDetails.observe(
                viewLifecycleOwner,
                { state -> handleResponse(state) }
            )
        }
    }

    private fun handleResponse(state: ApiState<UiPropertyDetails>) {
        when (state) {
            is ApiState.Loading -> onLoading()
            is ApiState.Error -> onErrorResponse(state.message)
            is ApiState.Success -> onSuccessResponse(state.data)
        }
    }

    private fun onLoading() {
        binding.fragmentDetailsContent.scContent.visibility = View.GONE
        binding.fragmentDetailsContent.cvActions.visibility = View.GONE
        binding.pBarLoading.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        binding.pBarLoading.visibility = View.GONE
    }

    private fun onErrorResponse(message: String) {
        hideLoading()
        hidePropertiesList()
        activity?.toast(message = message)
    }

    private fun onSuccessResponse(details: UiPropertyDetails) {
        hideLoading()
        showPropertiesList()

        binding.details = details

        mapView.showMarker(details.address.lat, details.address.lng)
    }

    private fun showPropertiesList() {
        binding.fragmentDetailsContent.scContent.visibility = View.VISIBLE
        binding.fragmentDetailsContent.cvActions.visibility = View.VISIBLE
    }

    private fun hidePropertiesList() {
        binding.fragmentDetailsContent.scContent.visibility = View.GONE
        binding.fragmentDetailsContent.cvActions.visibility = View.GONE
    }


    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }
}