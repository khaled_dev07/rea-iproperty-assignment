package com.rea.ipropertyassignment.presentation.listeners

interface RecyclerViewItemClickListener<in T> {
    fun onItemClicked(item: T, position: Int)
}