package com.rea.ipropertyassignment.presentation.model

data class UiItem(
    val channels: List<String>,
    val kind: String,
    val id: String,
    val shareLink: String,
    val title: String,
    val active: Boolean,
    val tier: Int,
    val propertyType: String,
    val prices: List<UiPrices>,
    val cover: UiCover?,
    val medias: List<UiCover>,
    val updatedAt: String,
    val publishedAt: String?,
    val address: UiAddress?,
    val multiLanguagePlace: UiMultiLanguagePlace,
    val referenceCode: String,
    val attributes: UiAttributes,
    val listers: List<UiListers>?,
    val organisations: List<UiOrganisations>?
)