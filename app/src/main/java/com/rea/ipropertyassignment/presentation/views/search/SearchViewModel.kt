package com.rea.ipropertyassignment.presentation.views.search

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rea.ipropertyassignment.domain.model.Properties
import com.rea.ipropertyassignment.domain.model.usecase.NetworkRequestParams
import com.rea.ipropertyassignment.domain.model.usecase.UseCaseResponse
import com.rea.ipropertyassignment.domain.usecase.PropertiesUseCase
import com.rea.ipropertyassignment.presentation.model.ApiState
import com.rea.ipropertyassignment.presentation.model.UiProperties
import com.rea.ipropertyassignment.presentation.views.base.BaseViewModel
import com.rea.ipropertyassignment.utils.Constants.PAGE_SIZE
import com.rea.ipropertyassignment.utils.mappers.UiPropertiesMapper
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    private val useCase: PropertiesUseCase,
    private val propertiesMapper: UiPropertiesMapper,
) : BaseViewModel() {

    companion object {
        private val TAG = SearchViewModel::class.java.simpleName
    }

    private val _searchedPropertyName = MutableLiveData<String>()
    val searchedPropertyName: LiveData<String>
        get() = _searchedPropertyName

    private val _propertiesList = MutableLiveData<ApiState<UiProperties>>()
    val propertiesList: LiveData<ApiState<UiProperties>>
        get() = _propertiesList


    fun setSearchedProperty(propertyName: String) {
        _searchedPropertyName.postValue(propertyName)
    }


    fun getProperties(page: Int = 0) {
        _propertiesList.postValue(ApiState.Loading)

        useCase.execute(
            params = NetworkRequestParams(page, PAGE_SIZE),
            compositeDisposable = compositeDisposable,
            onResponse = this::processResponse
        )
    }

    private fun processResponse(response: UseCaseResponse<Properties>) {
        Log.d(TAG, "processResponse() called  with: response")
        when (response) {
            is UseCaseResponse.SuccessResponse -> {
                val uiProperties = propertiesMapper.mapToUI(response.value)
                _propertiesList.postValue(ApiState.Success(uiProperties))
            }
            is UseCaseResponse.ErrorResponse -> {
                _propertiesList.postValue(ApiState.Error(response.error.message.toString()))
            }
        }
    }
}