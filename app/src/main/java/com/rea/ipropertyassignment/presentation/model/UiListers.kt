package com.rea.ipropertyassignment.presentation.model

data class UiListers(
	val id: Int?,
	val type: String?,
	val name: String?,
	val contact: UiContact?,
	val image: UiCover?
)