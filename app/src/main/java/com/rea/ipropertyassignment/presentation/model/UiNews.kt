package com.rea.ipropertyassignment.presentation.model

data class UiNews(
	val id: Int = 0,
	val title: String = "",
	val imageUrl: String = ""
)