package com.rea.ipropertyassignment.presentation.model

data class UiPhones(
	val label: String,
	val number: String
)