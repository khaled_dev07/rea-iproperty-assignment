package com.rea.ipropertyassignment.presentation.model

data class UiAttributes(
	val landArea: String?,
	val bathroom: String?,
	val bedroom: String?,
	val carPark: String?,
	val builtUp: String?,
	val landTitleType: String?,
	val furnishing: String?,
	val tenure: String?,
	val unitType: String?,
	val sizeUnit: String?,

	val downloadUrl: String?,
	val buildingId: Int?,
	val township: String?
)