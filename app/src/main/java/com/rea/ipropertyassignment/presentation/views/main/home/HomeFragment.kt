package com.rea.ipropertyassignment.presentation.views.main.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.rea.ipropertyassignment.databinding.FragmentHomeBinding
import com.rea.ipropertyassignment.domain.model.News
import com.rea.ipropertyassignment.presentation.listeners.RecyclerViewItemClickListener
import com.rea.ipropertyassignment.presentation.model.ApiState
import com.rea.ipropertyassignment.presentation.views.base.BindingFragment
import com.rea.ipropertyassignment.presentation.views.search.SearchFragment
import com.rea.ipropertyassignment.utils.Navigator
import com.rea.ipropertyassignment.utils.extensions.injectViewModel
import com.rea.ipropertyassignment.utils.extensions.setAllOnClickListener
import com.rea.ipropertyassignment.utils.extensions.setupLayout
import javax.inject.Inject

class HomeFragment : BindingFragment<FragmentHomeBinding>(), RecyclerViewItemClickListener<News> {

    companion object {
        fun newInstance() = HomeFragment()
    }

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    private val viewModel: HomeViewModel by lazy { injectViewModel(factory) }

    private lateinit var adapter: NewsAdapter


    override fun setupViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentHomeBinding {
        return FragmentHomeBinding.inflate(inflater, container, false)
    }

    override fun onFragmentCreated(savedInstanceState: Bundle?) {
        binding.layoutHomeSearchCard.txtSearch.setOnClickListener {
            openSearchFragment()
        }

        binding.fragmentNewsContent.layoutHomeCoverNews.group.setAllOnClickListener {
            openSearchFragment()
        }

        adapter = NewsAdapter(this)
        binding.fragmentNewsContent.layoutHomeCoverNews.rvNews.setupLayout(LinearLayoutManager.HORIZONTAL)
        binding.fragmentNewsContent.layoutHomeCoverNews.rvNews.adapter = adapter
    }

    override fun initViewModel() {
        with(viewModel) {
            newsTopCover.observe(
                viewLifecycleOwner,
                { state -> handleNewsTopCover(state) }
            )
            newsList.observe(
                viewLifecycleOwner,
                { state -> handleNewsList(state) }
            )
        }
    }

    override fun fetchData() {
        viewModel.getAvailableNews()
    }


    private fun handleNewsTopCover(state: ApiState<News>) {
        when (state) {
            is ApiState.Success -> showNewsTopCover(state.data)
        }
    }

    private fun showNewsTopCover(news: News) {
        binding.fragmentNewsContent.layoutHomeCoverNews.txtTopCover.text = news.title
        Glide.with(binding.fragmentNewsContent.layoutHomeCoverNews.imgTopCover.context)
            .load(news.imageUrl)
            .centerCrop()
            .placeholder(android.R.drawable.progress_indeterminate_horizontal)
            .error(android.R.drawable.stat_notify_error)
            .into(binding.fragmentNewsContent.layoutHomeCoverNews.imgTopCover)
    }


    private fun handleNewsList(state: ApiState<List<News>>) {
        when (state) {
            is ApiState.Success -> showNewsList(state.data)
        }
    }

    private fun showNewsList(list: List<News>) {
        adapter.submitList(list)
    }


    override fun onItemClicked(item: News, position: Int) {
        openSearchFragment()
    }

    private fun openSearchFragment(propertyName: String = "Arcoris Soho") {
        Navigator.with(activity).open(SearchFragment.newInstance(propertyName), true)
    }
}