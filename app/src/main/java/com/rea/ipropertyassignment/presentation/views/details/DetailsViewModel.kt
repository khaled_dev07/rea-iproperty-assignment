package com.rea.ipropertyassignment.presentation.views.details

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rea.ipropertyassignment.domain.model.PropertyDetails
import com.rea.ipropertyassignment.domain.model.usecase.PropertyDetailsRequestParams
import com.rea.ipropertyassignment.domain.model.usecase.UseCaseResponse
import com.rea.ipropertyassignment.domain.usecase.PropertyDetailsUseCase
import com.rea.ipropertyassignment.presentation.model.ApiState
import com.rea.ipropertyassignment.presentation.model.UiPropertyDetails
import com.rea.ipropertyassignment.presentation.views.base.BaseViewModel
import com.rea.ipropertyassignment.utils.mappers.UiPropertyDetailsMapper
import javax.inject.Inject

class DetailsViewModel @Inject constructor(
    private val useCase: PropertyDetailsUseCase,
    private val propertyDetailsMapper: UiPropertyDetailsMapper,
) : BaseViewModel() {

    companion object {
        private val TAG = DetailsViewModel::class.java.simpleName
    }

    private val _propertyDetails = MutableLiveData<ApiState<UiPropertyDetails>>()
    val propertyDetails: LiveData<ApiState<UiPropertyDetails>>
        get() = _propertyDetails


    fun getPropertyDetails(id: String) {
        _propertyDetails.postValue(ApiState.Loading)

        compositeDisposable.add(
            useCase.execute(
                params = PropertyDetailsRequestParams(id),
                compositeDisposable = compositeDisposable,
                onResponse = this::processResponse
            )
        )
    }

    private fun processResponse(response: UseCaseResponse<PropertyDetails>) {
        Log.d(TAG, "processResponse() called  with: response")
        when (response) {
            is UseCaseResponse.SuccessResponse -> {
                val uiPropertyDetails = propertyDetailsMapper.mapToUI(response.value)
                _propertyDetails.postValue(ApiState.Success(uiPropertyDetails))
            }
            is UseCaseResponse.ErrorResponse -> {
                _propertyDetails.postValue(ApiState.Error(response.error.message.toString()))
            }
        }
    }
}