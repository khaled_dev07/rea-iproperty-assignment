package com.rea.ipropertyassignment.presentation.views.main.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.rea.ipropertyassignment.databinding.FragmentProfileBinding
import com.rea.ipropertyassignment.presentation.views.base.BindingFragment
import com.rea.ipropertyassignment.presentation.views.main.home.HomeFragment

class ProfileFragment : BindingFragment<FragmentProfileBinding>() {

    companion object {
        fun newInstance() = HomeFragment()
    }

    override fun setupViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentProfileBinding {
        return FragmentProfileBinding.inflate(inflater, container, false)
    }

    override fun onFragmentCreated(savedInstanceState: Bundle?) {
    }

    override fun initViewModel() {
    }

    override fun fetchData() {
    }

}