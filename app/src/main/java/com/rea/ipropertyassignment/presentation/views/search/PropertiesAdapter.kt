package com.rea.ipropertyassignment.presentation.views.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rea.ipropertyassignment.databinding.PropertiesListItemBinding
import com.rea.ipropertyassignment.presentation.listeners.RecyclerViewItemClickListener
import com.rea.ipropertyassignment.presentation.model.UiItem

class PropertiesAdapter(
    private val items: ArrayList<UiItem?>,
    private val itemClickListener: RecyclerViewItemClickListener<UiItem>
) : RecyclerView.Adapter<PropertiesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PropertiesViewHolder {
        val binding = PropertiesListItemBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return PropertiesViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: PropertiesViewHolder, position: Int) {
        viewHolder.bind(items[position], itemClickListener)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun addItems(newItems: List<UiItem>) {
        this.items.addAll(newItems)
        notifyDataSetChanged()
    }
}