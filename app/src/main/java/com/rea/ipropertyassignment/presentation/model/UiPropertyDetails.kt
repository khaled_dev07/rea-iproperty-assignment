package com.rea.ipropertyassignment.presentation.model

data class UiPropertyDetails(
    val channels: List<String>,
    val kind: String,
    val id: String,
    val shareLink: String,
    val title: String,
    val tier: Int,
    val propertyType: String,
    val propertyCatType: String,
    val active: Boolean,
    val prices: List<UiPrices>,
    val medias: List<UiCover>,
    val cover: UiCover?,
    val description: String,
    val updatedAt: String,
    val address: UiAddress,
    val multiLanguagePlace: UiMultiLanguagePlace,
    val featureDescription: String,
    val referenceCode: String,
    val attributes: UiAttributes,
    val listers: List<UiListers>,
    val organisations: List<UiOrganisations>
) {
    val coverThumbnailUrl: String?
        get() = cover?.thumbnailUrl

    val mediaSize: String
        get() = String.format("%d", medias.size)

    val price: String
        get() = String.format("%s %d", prices[0].currency, prices[0].min)

    val address2: String
        get() = String.format(
            "%s %s", multiLanguagePlace.enGB?.levelTwo, multiLanguagePlace.enGB?.levelOne,
        )

    val space: String
        get() = String.format("built-up Size: %s", attributes.builtUp)


    val landTitle: String
        get() = attributes.landTitleType?.let { return it } ?: ""

    val unitType: String
        get() = attributes.unitType?.let { return it } ?: ""

    val tenure: String
        get() = attributes.tenure ?: ""

    val furnishing: String
        get() = attributes.furnishing ?: ""


    val agentName: String
        get() = listers[0].name ?: ""

    val agentProfileImageUrl: String
        get() = listers[0].image?.thumbnailUrl ?: ""

    val agentTitle: String
        get() = organisations[0].name
}
