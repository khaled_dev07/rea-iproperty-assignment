package com.rea.ipropertyassignment.presentation.model

data class UiEnGB(
    val levelOne: String,
    val levelTwo: String,
    val levelThree: String?
)