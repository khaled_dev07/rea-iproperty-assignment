package com.rea.ipropertyassignment.presentation.model

data class UiPrices(
	val type: String,
	val currency: String,
	val max: Int,
	val min: Int
)