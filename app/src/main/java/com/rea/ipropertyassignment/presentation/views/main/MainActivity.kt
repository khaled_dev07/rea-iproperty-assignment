package com.rea.ipropertyassignment.presentation.views.main

import android.os.Bundle
import android.view.LayoutInflater
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.rea.ipropertyassignment.R
import com.rea.ipropertyassignment.databinding.ActivityMainBinding
import com.rea.ipropertyassignment.presentation.views.base.BindingActivity
import com.rea.ipropertyassignment.presentation.views.main.home.HomeFragment
import com.rea.ipropertyassignment.presentation.views.main.profile.ProfileFragment
import com.rea.ipropertyassignment.presentation.views.main.saved.SavedFragment
import com.rea.ipropertyassignment.utils.Navigator

class MainActivity : BindingActivity<ActivityMainBinding>() {

    override fun setupViewBinding(inflater: LayoutInflater): ActivityMainBinding {
        return ActivityMainBinding.inflate(inflater)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            initBottomNavigationView()
            Navigator.with(this).open(HomeFragment(), true)
        }
    }

    private fun initBottomNavigationView() {
        binding.bnvMain.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.navigation_home -> {
                    Navigator.with(this).open(HomeFragment())
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_saved -> {
                    Navigator.with(this).open(SavedFragment())
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_profile -> {
                    Navigator.with(this).open(ProfileFragment())
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        })
    }


    override fun initViewModel() {
    }

    override fun fetchData() {
    }

    
    private fun selectHomeFragment() {
        binding.bnvMain.selectedItemId = R.id.navigation_home
    }

    override fun onBackPressed() {
        val isHomeSelected = R.id.navigation_home == binding.bnvMain.selectedItemId
        Navigator.with(this)
            .onBackPressed(
                isHomeSelected = isHomeSelected,
                navigateTo = { selectHomeFragment() }
            )
    }
}