package com.rea.ipropertyassignment.presentation.views.main.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rea.ipropertyassignment.domain.model.News
import com.rea.ipropertyassignment.domain.model.usecase.NetworkRequestParams
import com.rea.ipropertyassignment.domain.model.usecase.UseCaseResponse
import com.rea.ipropertyassignment.domain.usecase.NewsUseCase
import com.rea.ipropertyassignment.presentation.model.ApiState
import com.rea.ipropertyassignment.presentation.views.base.BaseViewModel
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val useCase: NewsUseCase,
) : BaseViewModel() {

    companion object {
        private val TAG = HomeViewModel::class.java.simpleName
    }

    private val _newsTopCover = MutableLiveData<ApiState<News>>()
    val newsTopCover: LiveData<ApiState<News>>
        get() = _newsTopCover

    private val _newsList = MutableLiveData<ApiState<List<News>>>()
    val newsList: LiveData<ApiState<List<News>>>
        get() = _newsList


    fun getAvailableNews() {
        _newsList.postValue(ApiState.Loading)

        useCase.execute(
            params = NetworkRequestParams(),
            compositeDisposable = compositeDisposable,
            onResponse = this::processResponse
        )
    }

    private fun processResponse(response: UseCaseResponse<List<News>>) {
        Log.d(TAG, "processResponse() called  with: response")
        when (response) {
            is UseCaseResponse.SuccessResponse -> {
                _newsTopCover.postValue(ApiState.Success(response.value.first()))
                _newsList.postValue(ApiState.Success(response.value.drop(1)))
            }
            is UseCaseResponse.ErrorResponse -> {
                _newsTopCover.postValue(ApiState.Error(response.error.message.toString()))
                _newsList.postValue(ApiState.Error(response.error.message.toString()))
            }
        }
    }
}