package com.rea.ipropertyassignment.presentation.model

data class UiAddress(
	val formattedAddress: String,
	val lat: Double,
	val lng: Double
)