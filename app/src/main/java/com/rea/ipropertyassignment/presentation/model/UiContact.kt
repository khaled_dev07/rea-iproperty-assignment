package com.rea.ipropertyassignment.presentation.model

data class UiContact(
	val phones: List<UiPhones>
)