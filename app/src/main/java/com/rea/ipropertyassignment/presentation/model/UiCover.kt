package com.rea.ipropertyassignment.presentation.model

data class UiCover(
	val type: String,
	val url: String?,
	val thumbnailUrl: String,
	val urlTemplate: String
)