package com.rea.ipropertyassignment.di.module

import android.app.Application
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import com.rea.ipropertyassignment.BuildConfig
import com.rea.ipropertyassignment.data.repository.Repository
import com.rea.ipropertyassignment.data.repository.RepositoryImpl
import com.rea.ipropertyassignment.domain.network.ApiInterface
import com.rea.ipropertyassignment.domain.network.MockRequestInterceptor
import com.rea.ipropertyassignment.utils.Constants
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.internal.platform.Platform
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun providesGson(): Gson {
        return Gson()
    }

    @Provides
    @Singleton
    fun providesGsonConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create(
            GsonBuilder().setPrettyPrinting().create()
        )
    }

    @Provides
    @Singleton
    fun providesRxJavaCallAdapterFactory(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.create()
    }

    @Provides
    @Singleton
    fun providesOkHttpClient(context: Application): OkHttpClient {
        val interceptor: Interceptor = LoggingInterceptor.Builder()
            .loggable(BuildConfig.DEBUG)
            .setLevel(Level.BASIC)
            .log(Platform.INFO)
            .request("Request")
            .response("Response")
            .addHeader("version", BuildConfig.VERSION_NAME)
            .build()

        val client = OkHttpClient.Builder()
        client.readTimeout(Constants.TIMEOUT.toLong(), TimeUnit.SECONDS)
        client.connectTimeout(Constants.TIMEOUT.toLong(), TimeUnit.SECONDS)
        client.addInterceptor(interceptor)
        client.addInterceptor(MockRequestInterceptor(context))

        return client.build()
    }

    @Provides
    @Singleton
    fun providesRetrofit(
        gsonConverterFactory: GsonConverterFactory,
        rxJava2CallAdapterFactory: RxJava2CallAdapterFactory,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(rxJava2CallAdapterFactory)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun providesApiInterface(retrofit: Retrofit): ApiInterface {
        return retrofit.create(ApiInterface::class.java)
    }


    @Provides
    @Singleton
    fun providesDataSource(): CompositeDisposable {
        return CompositeDisposable()
    }


    @Provides
    @Singleton
    fun providesRepository(
        apiInterface: ApiInterface
    ): Repository {
        return RepositoryImpl(apiInterface)
    }
}