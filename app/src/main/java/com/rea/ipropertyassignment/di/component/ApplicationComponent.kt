package com.rea.ipropertyassignment.di.component

import android.app.Application
import com.rea.ipropertyassignment.AppController
import com.rea.ipropertyassignment.di.module.ActivityModule
import com.rea.ipropertyassignment.di.module.ApplicationModule
import com.rea.ipropertyassignment.di.module.FragmentModule
import com.rea.ipropertyassignment.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ApplicationModule::class,
        ActivityModule::class,
        FragmentModule::class,
        ViewModelModule::class,
    ]
)
interface ApplicationComponent : AndroidInjector<AppController> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    override fun inject(instance: AppController)
}