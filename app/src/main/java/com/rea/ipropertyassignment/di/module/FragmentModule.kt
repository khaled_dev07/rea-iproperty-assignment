package com.rea.ipropertyassignment.di.module

import com.rea.ipropertyassignment.presentation.views.details.DetailsFragment
import com.rea.ipropertyassignment.presentation.views.main.home.HomeFragment
import com.rea.ipropertyassignment.presentation.views.main.profile.ProfileFragment
import com.rea.ipropertyassignment.presentation.views.main.saved.SavedFragment
import com.rea.ipropertyassignment.presentation.views.search.SearchFragment
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = [AndroidInjectionModule::class])
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun bindHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun bindSavedFragment(): SavedFragment

    @ContributesAndroidInjector
    abstract fun bindProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun bindSearchFragment(): SearchFragment

    @ContributesAndroidInjector
    abstract fun bindDetailsFragment(): DetailsFragment
}