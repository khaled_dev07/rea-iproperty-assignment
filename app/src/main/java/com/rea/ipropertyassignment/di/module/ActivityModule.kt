package com.rea.ipropertyassignment.di.module

import com.rea.ipropertyassignment.presentation.views.main.MainActivity

import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = [AndroidInjectionModule::class])
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun bindHomeActivity(): MainActivity
}