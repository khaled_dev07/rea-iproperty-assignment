package com.rea.ipropertyassignment

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import com.rea.ipropertyassignment.di.component.ApplicationComponent
import com.rea.ipropertyassignment.di.component.DaggerApplicationComponent

class AppController : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = buildApplicationComponent()
        appComponent.inject(this)
        return appComponent
    }

    private fun buildApplicationComponent(): ApplicationComponent {
        return DaggerApplicationComponent.builder()
            .application(this)
            .build()
    }
}