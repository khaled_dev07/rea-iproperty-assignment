package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.Organisations
import com.rea.ipropertyassignment.presentation.model.UiOrganisations
import javax.inject.Inject

class UiOrganisationsMapper @Inject constructor() : Mapper<List<Organisations>, List<UiOrganisations>> {

    override fun mapToUI(type: List<Organisations>): List<UiOrganisations> {
        val organisations = mutableListOf<UiOrganisations>()
        type.forEach {
            organisations.add(
                UiOrganisations(
                    id = it.id,
                    type = it.type,
                    name = it.name
                )
            )
        }

        return organisations
    }
}