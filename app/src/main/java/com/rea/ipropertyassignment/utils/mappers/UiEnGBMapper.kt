package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.EnGB
import com.rea.ipropertyassignment.presentation.model.UiEnGB
import javax.inject.Inject

class UiEnGBMapper @Inject constructor() : Mapper<EnGB, UiEnGB> {

    override fun mapToUI(type: EnGB): UiEnGB {
        return UiEnGB(
            levelOne = type.levelOne,
            levelTwo = type.levelTwo,
            levelThree = type.levelThree
        )
    }
}