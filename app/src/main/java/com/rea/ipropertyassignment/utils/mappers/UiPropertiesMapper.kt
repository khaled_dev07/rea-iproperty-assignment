package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.Properties
import com.rea.ipropertyassignment.presentation.model.UiProperties
import javax.inject.Inject

class UiPropertiesMapper @Inject constructor(
    private val propertyItemsMapper: UiPropertyItemsMapper,
) : Mapper<Properties, UiProperties> {

    override fun mapToUI(type: Properties): UiProperties {
        return UiProperties(
            totalCount = type.totalCount,
            nextPageToken = type.nextPageToken,
            items = propertyItemsMapper.mapToUI(type.items)
        )
    }
}