package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.Cover
import com.rea.ipropertyassignment.presentation.model.UiCover
import javax.inject.Inject

class UiCoversMapper @Inject constructor() : Mapper<List<Cover>, List<UiCover>> {

    override fun mapToUI(type: List<Cover>): List<UiCover> {
        val medias = mutableListOf<UiCover>()
        type.forEach {
            medias.add(
                UiCover(
                    type = it.type,
                    url = it.url,
                    thumbnailUrl = it.thumbnailUrl,
                    urlTemplate = it.urlTemplate
                )
            )
        }

        return medias
    }
}