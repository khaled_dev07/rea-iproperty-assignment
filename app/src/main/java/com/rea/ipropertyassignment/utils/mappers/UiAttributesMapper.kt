package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.Attributes
import com.rea.ipropertyassignment.presentation.model.UiAttributes
import javax.inject.Inject

class UiAttributesMapper @Inject constructor() : Mapper<Attributes, UiAttributes> {

    override fun mapToUI(type: Attributes): UiAttributes {
        return UiAttributes(
            landArea = type.landArea,
            bathroom = type.bathroom,
            bedroom = type.bedroom,
            carPark = type.carPark,
            builtUp = type.builtUp,
            landTitleType = type.landTitleType,
            furnishing = type.furnishing,
            tenure = type.tenure,
            unitType = type.unitType,
            sizeUnit = type.sizeUnit,
            downloadUrl = type.downloadUrl,
            buildingId = type.buildingId,
            township = type.township
        )
    }
}