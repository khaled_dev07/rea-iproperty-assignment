package com.rea.ipropertyassignment.utils.extensions

import android.content.Context
import android.os.Bundle
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

fun MapView.init(context: Context, savedInstanceState: Bundle?) {
    onCreate(savedInstanceState)
    onResume()

    try {
        MapsInitializer.initialize(context)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun MapView.showMarker(lat: Double, lng: Double) {
    getMapAsync { map ->
        val marker = LatLng(lat, lng)
        map.addMarker(MarkerOptions().position(marker))

        val cameraPosition = CameraPosition.Builder().target(marker).zoom(12f).build()
        map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }
}