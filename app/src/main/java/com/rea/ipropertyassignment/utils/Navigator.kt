package com.rea.ipropertyassignment.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.rea.ipropertyassignment.R
import com.rea.ipropertyassignment.presentation.views.main.home.HomeFragment

object Navigator {

    private var activity: FragmentActivity? = null

    fun with(activity: FragmentActivity?): Navigator {
        this.activity = activity
        return this@Navigator
    }

    fun open(fragment: Fragment, addToBackStack: Boolean = false) {
        activity?.open(fragment, addToBackStack)
    }

    fun popFragment() {
        activity?.popFragment()
    }

    fun onBackPressed(isHomeSelected: Boolean = true, navigateTo: () -> Unit) {
        val currentFragment = fragments.last()

        if (isHomeSelected) {
            if (currentFragment is HomeFragment) {
                fragments.clear()
                activity?.finish()
            } else {
                activity?.popFragment()
            }
        } else {
            navigateTo.invoke()
        }
    }
}


val fragments = mutableListOf<Fragment>()

fun FragmentActivity.open(fragment: Fragment, addToBackStack: Boolean) {
    val tag = fragment.javaClass.simpleName
    val currentFragment: Fragment?

    if (fragment is HomeFragment && fragments.size > 0) {
        currentFragment = fragments.last()
    } else {
        currentFragment = supportFragmentManager.findFragmentByTag(tag)
    }

    supportFragmentManager.beginTransaction().apply {
        currentFragment?.let {
            if (it.isAdded) {
                show(it)
            }
        } ?: run {
            add(R.id.fr_container, fragment, tag)
            if (addToBackStack) {
                fragments.add(fragment)
            }
        }

        supportFragmentManager.fragments.forEach {
            if (it != currentFragment && it.isAdded) {
                hide(it)
            }
        }
    }.commit()

    supportFragmentManager.executePendingTransactions()
}

fun FragmentActivity.popFragment() {
    val currentFragment = fragments.last()
    fragments.removeIf { f -> f.tag == currentFragment.tag }

    val trans: FragmentTransaction = supportFragmentManager.beginTransaction()
    trans.remove(currentFragment)
    trans.commit()
    supportFragmentManager.popBackStack()

    val previousFragment = fragments.last()
    open(previousFragment, false)
}
