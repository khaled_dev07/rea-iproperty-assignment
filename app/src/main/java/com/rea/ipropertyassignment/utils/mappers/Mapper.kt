package com.rea.ipropertyassignment.utils.mappers

interface Mapper<in T, out R> {

    fun mapToUI(type: T): R
}