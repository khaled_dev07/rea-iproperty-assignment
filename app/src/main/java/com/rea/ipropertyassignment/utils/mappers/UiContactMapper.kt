package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.Contact
import com.rea.ipropertyassignment.presentation.model.UiContact
import javax.inject.Inject

class UiContactMapper @Inject constructor(
    private val phonesMapper: UiPhonesMapper,
) : Mapper<Contact, UiContact> {

    override fun mapToUI(type: Contact): UiContact {
        return UiContact(phones = phonesMapper.mapToUI(type.phones))
    }
}