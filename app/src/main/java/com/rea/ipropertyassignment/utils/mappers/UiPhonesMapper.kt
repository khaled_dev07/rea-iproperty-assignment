package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.Phones
import com.rea.ipropertyassignment.presentation.model.UiPhones
import javax.inject.Inject

class UiPhonesMapper @Inject constructor() : Mapper<List<Phones>, List<UiPhones>> {

    override fun mapToUI(type: List<Phones>): List<UiPhones> {
        val phones = mutableListOf<UiPhones>()
        type.forEach { phone ->
            phones.add(
                UiPhones(
                    label = phone.label,
                    number = phone.number
                )
            )
        }

        return phones
    }
}