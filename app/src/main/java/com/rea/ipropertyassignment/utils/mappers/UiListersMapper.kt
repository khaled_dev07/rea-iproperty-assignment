package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.Listers
import com.rea.ipropertyassignment.presentation.model.UiListers
import javax.inject.Inject

class UiListersMapper @Inject constructor(
    private val contactMapper: UiContactMapper,
    private val coverMapper: UiCoverMapper,
) : Mapper<List<Listers>, List<UiListers>> {

    override fun mapToUI(type: List<Listers>): List<UiListers> {
        val listers = mutableListOf<UiListers>()
        type.forEach {
            listers.add(
                UiListers(
                    id = it.id,
                    type = it.type,
                    name = it.name,
                    contact = it.contact?.let { it1 -> contactMapper.mapToUI(it1) },
                    image = it.image?.let { img -> coverMapper.mapToUI(img) }
                )
            )
        }

        return listers
    }
}