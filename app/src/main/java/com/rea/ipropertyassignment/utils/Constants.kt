package com.rea.ipropertyassignment.utils

object Constants {
    const val TIMEOUT = 60  /* in Seconds */
    const val PAGE_SIZE = 20
}