package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.Address
import com.rea.ipropertyassignment.presentation.model.UiAddress
import javax.inject.Inject

class UiAddressMapper @Inject constructor() : Mapper<Address, UiAddress> {

    override fun mapToUI(type: Address): UiAddress {
        return UiAddress(
            formattedAddress = type.formattedAddress,
            lat = type.lat,
            lng = type.lng
        )
    }
}