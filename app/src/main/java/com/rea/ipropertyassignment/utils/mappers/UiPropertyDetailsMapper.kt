package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.PropertyDetails
import com.rea.ipropertyassignment.presentation.model.UiPropertyDetails
import javax.inject.Inject

class UiPropertyDetailsMapper @Inject constructor(
    private val pricesMapper: UiPricesMapper,
    private val coversMapper: UiCoversMapper,
    private val coverMapper: UiCoverMapper,
    private val addressMapper: UiAddressMapper,
    private val multiLanguagePlaceMapper: UiMultiLanguagePlaceMapper,
    private val attributesMapper: UiAttributesMapper,
    private val listersMapper: UiListersMapper,
    private val organisationsMapper: UiOrganisationsMapper,
) : Mapper<PropertyDetails, UiPropertyDetails> {

    override fun mapToUI(type: PropertyDetails): UiPropertyDetails {
        return UiPropertyDetails(
            channels = type.channels,
            kind = type.kind,
            id = type.id,
            shareLink = type.shareLink,
            title = type.title,
            tier = type.tier,
            propertyType = type.propertyType,
            propertyCatType = type.propertyCatType,
            active = type.active,
            prices = pricesMapper.mapToUI(type.prices),
            medias = coversMapper.mapToUI(type.medias),
            cover = type.cover?.let { coverMapper.mapToUI(it) },
            description = type.description,
            updatedAt = type.updatedAt,
            address = addressMapper.mapToUI(type.address),
            multiLanguagePlace = multiLanguagePlaceMapper.mapToUI(type.multiLanguagePlace),
            featureDescription = type.featureDescription,
            referenceCode = type.referenceCode,
            attributes = attributesMapper.mapToUI(type.attributes),
            listers = listersMapper.mapToUI(type.listers),
            organisations = organisationsMapper.mapToUI(type.organisations)
        )
    }
}