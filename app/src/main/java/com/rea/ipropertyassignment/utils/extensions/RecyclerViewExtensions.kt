package com.rea.ipropertyassignment.utils.extensions

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.setupLayout(
    direction: Int = RecyclerView.VERTICAL,
    reverse: Boolean = false,
    hasFixedSize: Boolean = true
) {
    layoutManager = LinearLayoutManager(context, direction, reverse)
    setHasFixedSize(hasFixedSize)
}


fun RecyclerView.setupLayout(
    layout: LinearLayoutManager,
    hasFixedSize: Boolean = true
) {
    layoutManager = layout
    setHasFixedSize(hasFixedSize)
}
