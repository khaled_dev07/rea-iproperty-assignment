package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.Prices
import com.rea.ipropertyassignment.presentation.model.UiPrices
import javax.inject.Inject

class UiPricesMapper @Inject constructor() :
    Mapper<List<Prices>, List<UiPrices>> {
    override fun mapToUI(type: List<Prices>): List<UiPrices> {
        val prices = mutableListOf<UiPrices>()
        type.forEach {
            prices.add(
                UiPrices(
                    type = it.type,
                    currency = it.currency,
                    max = it.max,
                    min = it.min
                )
            )
        }

        return prices
    }
}