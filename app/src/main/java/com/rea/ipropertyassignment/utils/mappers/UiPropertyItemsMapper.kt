package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.Item
import com.rea.ipropertyassignment.presentation.model.UiItem
import javax.inject.Inject

class UiPropertyItemsMapper @Inject constructor(
    private val pricesMapper: UiPricesMapper,
    private val coversMapper: UiCoversMapper,
    private val coverMapper: UiCoverMapper,
    private val addressMapper: UiAddressMapper,
    private val multiLanguagePlaceMapper: UiMultiLanguagePlaceMapper,
    private val attributesMapper: UiAttributesMapper,
    private val listersMapper: UiListersMapper,
    private val organisationsMapper: UiOrganisationsMapper,
) : Mapper<List<Item>, List<UiItem>> {

    override fun mapToUI(type: List<Item>): List<UiItem> {
        val items = mutableListOf<UiItem>()
        type.forEach { item ->
            items.add(
                UiItem(
                    channels = item.channels,
                    kind = item.kind,
                    id = item.id,
                    shareLink = item.shareLink,
                    title = item.title,
                    active = item.active,
                    tier = item.tier,
                    propertyType = item.propertyType,
                    prices = pricesMapper.mapToUI(item.prices),
                    cover = item.cover?.let { coverMapper.mapToUI(it) },
                    medias = coversMapper.mapToUI(item.medias),
                    updatedAt = item.updatedAt,
                    publishedAt = item.publishedAt,
                    address = item.address?.let { addressMapper.mapToUI(it) },
                    multiLanguagePlace = multiLanguagePlaceMapper.mapToUI(item.multiLanguagePlace),
                    referenceCode = item.referenceCode,
                    attributes = attributesMapper.mapToUI(item.attributes),
                    listers = item.listers?.let { listersMapper.mapToUI(it) },
                    organisations = item.organisations?.let { organisationsMapper.mapToUI(it) }
                )
            )
        }

        return items
    }
}