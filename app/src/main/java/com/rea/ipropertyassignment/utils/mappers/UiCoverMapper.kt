package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.Cover
import com.rea.ipropertyassignment.presentation.model.UiCover
import javax.inject.Inject

class UiCoverMapper @Inject constructor() : Mapper<Cover, UiCover> {

    override fun mapToUI(type: Cover): UiCover {
        return UiCover(
            type = type.type,
            url = type.url,
            thumbnailUrl = type.thumbnailUrl,
            urlTemplate = type.urlTemplate
        )
    }
}