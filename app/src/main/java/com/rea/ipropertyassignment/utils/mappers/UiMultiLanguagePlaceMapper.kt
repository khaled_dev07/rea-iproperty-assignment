package com.rea.ipropertyassignment.utils.mappers

import com.rea.ipropertyassignment.domain.model.MultiLanguagePlace
import com.rea.ipropertyassignment.presentation.model.UiMultiLanguagePlace
import javax.inject.Inject

class UiMultiLanguagePlaceMapper @Inject constructor(
    private val enGBMapper: UiEnGBMapper,

    ) : Mapper<MultiLanguagePlace, UiMultiLanguagePlace> {

    override fun mapToUI(type: MultiLanguagePlace): UiMultiLanguagePlace {
        return UiMultiLanguagePlace(
            enGB = type.enGB?.let { enGBMapper.mapToUI(it) }
        )
    }
}